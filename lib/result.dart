import 'package:flutter/material.dart';

class Result extends StatelessWidget {
  final int resultScore;
  final VoidCallback resetHandler;
  Result(this.resultScore, this.resetHandler);

  String get resultPhrase {
    String resultText;
    if (resultScore <= 8) {
      resultText = 'nice';
    } else if (resultScore <= 12) {
      resultText = 'so nice';
    } else if (resultScore <= 16) {
      resultText = 'very nice';
    } else {
      resultText = 'bad!!';
    }
    return resultText + '   ' + resultScore.toString();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: [
          Text(
            resultPhrase,
            style: const TextStyle(fontSize: 36, fontWeight: FontWeight.bold),
          ),
          // FlatButton(
          //   child: Text("Restart Quiz"),
          //   onPressed: resetHandler,
          //   textColor: Colors.blue,
          // )
          TextButton(
            onPressed: resetHandler,
            child: Text("Restart Quiz"),
            style: ButtonStyle(
                foregroundColor: MaterialStateProperty.all(Colors.blue)),
          )
        ],
      ),
    );
  }
}
